/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


function promptFirst() {

			let fullName = prompt("Enter your Full Name: ");
			let age = prompt("Enter your Age: ");
			let location = prompt("Enter your Location: ");

			console.log("Full Name: " + fullName);
			console.log("Your age: " + age);
			console.log("Your location: " + location);
	}

promptFirst();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function topBands() {

	console.log("Top 1: Cityalight");
	console.log("Top 2: Paramore");
	console.log("Top 3: Bruno Major");
	console.log("Top 4: Adie");
	console.log("Top 5: Ben&Ben");
}

topBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function topMovies() {

	console.log("Top 1: Avengers End Game");
	console.log("Avengers End Game Rating: 94%");
	console.log("Top 2: Knives Out");
	console.log("Knives Out Rating: 97%");
	console.log("Top 3: My Sassy Girl");
	console.log("My Sassy Girl Rating: 93%")
	console.log("Top 4: Iron-Man");
	console.log("Iron-Man Rating: 94%")
	console.log("Top 5: Fast and Furious 5");
	console.log("Fast and Furious 5 Rating: 78%")
}

topMovies();



// 	4. Debugging Practice - Debug the following codes and functions to avoid errors.
// 		-check the variable names
// 		-check the variable scope
// 		-check function invocation/declaration
// 		-comment out unusable codes.

alert("Hi! Please add the names of your friends.");
let printFriends = function printUsers(){
	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();